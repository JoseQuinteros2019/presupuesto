import React from 'react';
import Badge from 'react-bootstrap/Badge';
import ListGroup from 'react-bootstrap/ListGroup';

const Gasto = (props) => {
    return (
        <ListGroup.Item>
        <div className="row">
            <div className="col-lg-6"><h1>{props.info.detalle} </h1></div>
            <div className="col-lg-6"><Badge variant="secondary">$ {props.info.monto} </Badge></div>
            
        </div>
        </ListGroup.Item>
    );
};

export default Gasto;
