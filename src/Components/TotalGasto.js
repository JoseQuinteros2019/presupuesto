import React from 'react';
import Alert from 'react-bootstrap/Alert';
// aqui creamos variable globales
let clases = "";
const Detalle = (props) => {
  if ((props.PresupuestoInicial / 4) > props.PresupuestoFinal)
  { clases = "danger" }
  else
  {
    if ((props.PresupuestoInicial / 2) >props.PresupuestoFinal)
    { clases = "warning" }  
    else {
      clases="success"
    }
  }
    return (
        <div>
         <Alert variant="success">
  
  <p className="mb-0">
   Presupuesto Inicial: {props.PresupuestoInicial}
  </p>
 
 

</Alert>
<Alert variant={clases}>
  
  <p className="mb-0">
   Presupuesto Restante: {props.PresupuestoFinal}
  </p>
 
 

</Alert>
        </div>
    );
};

export default Detalle;