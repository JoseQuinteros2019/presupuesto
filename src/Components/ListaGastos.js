import React from 'react';
import Gasto from './Gasto';
import ListGroup from 'react-bootstrap/ListGroup';

const ListaGastos = (props) => {
    return (
        <div>
            <ListGroup>
                {
props.ListadoGastos.map((dato,index)=><Gasto info={dato} key={index} />)
                }
               
               </ListGroup> 
        </div>
    );
};

export default ListaGastos;