import React,{Component} from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Error from './Error';
class DefinirGasto extends Component  {
  state = {
    detalle: '',
    monto: 0,
    error: false // cuando sea verdadero hay error
  }
  handleChange = (event) => {
    if (event.target.name === "monto")
    {this.setState({
// guardo una copia del state
      ...this.state, [event.target.name]: parseInt( event.target.value)
    })
      
    }
    else { 
    this.setState({

      ...this.state, [event.target.name]: event.target.value
    })
     }
    
  }
  handleSubmit = (event) => {
    event.preventDefault();
    // validar los campos detalle y monto
    if (this.state.detalle === '' || this.state.monto < 0)
    { this.setState({error:true})  }
    else
    {// antes de limpiar el state llamamos a la funcion
      this.props.guardarGasto(this.state)
      
      // limpiamos el state

      this.setState({
        detalle: '',
        monto: 0,
        error:false
      })
      }
  }
    
    render(){
  return (
    <div>
      <h1> Crear Gasto </h1>
      {
        (this.state.error)?<Error mensaje="complete todos los datos" />:null
      }
      
      <Form onSubmit={this.handleSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Nombre de Gastos</Form.Label>
          <Form.Control type="text" placeholder="nombre de gasto" name="detalle" onChange={this.handleChange} value={this.state.detalle} />
        
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Cantidad de Gastos</Form.Label>
          <Form.Control type="number" placeholder="monto del gasto" name="monto" onChange={this.handleChange} value={this.state.monto} />
        </Form.Group>
       
        <Button variant="primary" type="submit">
          Agregar Gasto
        </Button>
      </Form>
    </div>
  );
};
}

export default DefinirGasto;
