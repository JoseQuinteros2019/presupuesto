import React, { Component } from "react";
import PresupuestoInicial from "./Components/PresupuestoInicial";
import DefinirGasto from "./Components/DefinirGasto";
import ListaGastos from "./Components/ListaGastos";
import TotalGasto from "./Components/TotalGasto";
import Error from "./Components/Error";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  state = {
    presupuesto: 0,
    preguntapresupuesto: false, // cuando sea true tengo un presupuesto , si es falso el prespuesto es 0
    arregloGastos: [],
    presupuestoFinal:0
  };
  guardarPresupuesto = monto => {
    this.setState({
      presupuesto: monto,
      preguntapresupuesto: true,
      presupuestoFinal:monto
    });
  };
  guardarGasto = (gasto) => {
    let arregloProvisorio = this.state.arregloGastos;
    arregloProvisorio.push(gasto);
    this.setState({
      arregloGastos: arregloProvisorio
    });
    // aqui resto del presupuesto el valor del gasto y lo guardo en el final
    let diferencia = this.state.presupuestoFinal - gasto.monto;// gasto.monto viene del state de definirGasto
    this.setState({
      presupuestoFinal: diferencia
    })
    
  }
  render() {
    return (
      <div className="container mt-5">
        {this.state.preguntapresupuesto ? (
          <div className="row">
            <div className="col-sm-12 col-md-6"> <DefinirGasto guardarGasto={this.guardarGasto} /> </div>
            <div className="col-sm-12 col-md-6">  <ListaGastos ListadoGastos={this.state.arregloGastos} />
            <TotalGasto PresupuestoInicial={this.state.presupuesto} PresupuestoFinal={this.state.presupuestoFinal} /></div>
           
          </div>
        ) : (
          <PresupuestoInicial guardarPresupuesto={this.guardarPresupuesto} />
        )}
      </div>
    );
  }
}

export default App;
