import React,{Component} from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Error from './Error';


class PresupuestoInicial extends Component  {
  state = {
    montoinicial: 0,
    error:false,// cuando sea falso no hay error
  }
    handleChange=(e)=>{
        this.setState ({ montoinicial:parseInt(e.target.value)})

    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let montoinicial=this.state.montoinicial;
        if (montoinicial>0 || isNaN(montoinicial) )
         {
          this.setState({ error: false });
          // volvemos al estado inicial
          this.props.guardarPresupuesto(this.state.montoinicial) }// mando el montoinicial al componente app
        else {
          alert("no cumple con los requisitos");
          this.setState({
            error: true,
          })
          return;// sale del handle
      }

    }
    render(){
  return (
    <div>
      <h1 className="text-center">Coloca tu presupuesto</h1>
      {
        // aqui vienen un operador ternario
        this.state.error ?<Error mensaje={"el presupuesto es incorrecto"} />:null
      }
      
      <Form onSubmit={this.handleSubmit} >
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Presupuesto</Form.Label>
          <Form.Control type="number" placeholder="ingresa tu presupuesto" onChange={this.handleChange} />
         
        </Form.Group>

       
        <Button variant="primary" className="w-100" type="submit">
          Definir Presupuesto
        </Button>
      </Form>
    </div>
  );
};
}

export default PresupuestoInicial;
