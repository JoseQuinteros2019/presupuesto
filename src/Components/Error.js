import React from 'react';
import Alert from 'react-bootstrap/Alert';

const Error = (props) => {
    return (
        <div>
              <Alert variant="danger">
  
  <p className="mb-0">
  {props.mensaje}
  </p>
 
 

</Alert>
        </div>
    );
};

export default Error;